# FSGPS on Electron with React
Open Source version of Farm Sprayer GPS. Designed with React to run on Electron so it will work on any modern hardware. See the plantermonitor.com/store site to help support the project by buying hardware.

npm install

npm run build

npm run electron

Mapping is based on Leaflet with maps sourced from ESRI. This project is open source, HOWEVER, if you end up using this for commercial purposes you will need the proper ESRI developer subscription and proper map attributions. Because this project uses Leaflet you can easily substitute for Open Street Maps etc.

Package as Electron App -- 

electron-packager . FSGPS --platform=linux --arch=arm64

electron-packager . FSGPS --platform=linux --arch=armv7l

electron-packager . FSGPS --platform=darwin --arch=x64

************************

//Start up with Auto Login
sudo systemctl edit getty@tty1.service

This will the create a drop-in file (if neccessary) and open it an editor. Add the following, replacing myusername with your user name:

[Service]
ExecStart=
ExecStart=-/sbin/agetty --noissue --autologin ubuntu %I $TERM
Type=idle


sudo apt install xserver-xorg-video-fbdev

************************

Verify if serial port is working correctly.

echo -n "/dev/"; dmesg | grep tty|grep USB|rev|awk '{print $1}'|rev

************************

Raspbian Desktop Icon settings

File manager
Edit
Preferences
General
Do not ask option on executable launch

Desktop Launcher

[Desktop Entry]
Name=Farm Sprayer
Type=Application
Terminal=false
StartupNotify=false
Exec=/home/pi/openfsgps/FSGPS-linux-armv7l/FSGPS
Name[en_US]=FARM.desktop

Update Launcher

[Desktop Entry]
Name=Updater
Type=Application
Terminal=true
StartupNotify=false
Exec=/home/pi/openfsgps/updatescript
Name[en_US]=UPDATE.desktop

************************

Maybe Update script...

#!/bin/sh

cd /home/pi/openfsgps

./recovery

cd /home/pi/openfsgps

git pull

npm install

npm run rebuild

npm run build

electron-packager . FSGPSUPDATED --platform=linux --arch=armv7l --overwrite

sudo rm -R FSGPS-linux-armv7l/resources/*
sudo cp -R FSGPSUPDATED-linux-armv7l/resources/ FSGPS-linux-armv7l/

cd /home/pi/openfsgps

./recovery2

************************


