import React, { Component } from "react";
import { Button } from "@material-ui/core";
import homeicon from "./homeicon.svg";

class AntennaUtil extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "MPEP 100",
      numPages: null,
      pageNumber: 1,
      gpsDevices: 0,
        selectedDevice: "Select Device"
    };
  }

  componentDidMount() {

      try{
          var portPath =  localStorage.getItem("device");
          if(portPath == null || portPath == "") {}
          else{this.setState({selectedDevice: portPath})}
      }
      catch (e) {

          console.log("No Previous device")

      }

      try {

      window.ipcRenderer.send("gpsports", "a string", 10);

      window.ipcRenderer.on("gpsports", (event, messages) => {
        // do something
        console.log(messages);

        this.setState({ gpsDevices: messages });
      });

      /*
    window.ipcRenderer.on("gpsports", (event, message) => {
         console.log(message);
        // self.dataFeed(message);

    }); */

      //console.log(localStorage.getItem("usbdevices"))
    } catch (e) {
      console.log(e);
    }
  }

  deviceSelected = (path) => {

      localStorage.setItem("device", path )

      window.ipcRenderer.send("gpsselected", path, 10);

      this.setState({selectedDevice: path })

  }

    goHome = () => {
        this.props.history.push("/mainmenu");
    };

  render() {
    var TodoItems;

    if (this.state.gpsDevices == 0) {
      TodoItems = () => <div> No Devices</div>;
    } else {
      TodoItems = this.state.gpsDevices.map((devices, index) => (
        <Button onClick={() => this.deviceSelected(devices.path)}
          style={{
            backgroundColor: "grey",
            marginBottom: "5px",
            marginLeft: "5px",
            marginRight: "5px",
            display: "block",
          }}
          key={index}
        >
          {devices.path} {devices.manufacturer} {devices.productId}{" "}
          {devices.pnpId}
        </Button>
      ));
    }

    return (
      <div>
        Select GPS device.
        <br />
        <div style={{ marginTop: "25px" }}>
          <center>{TodoItems}</center>
        </div>
          {this.state.selectedDevice + " selected"}
          <Button
              onClick={() => this.goHome()}
              style={{
                  position: "absolute",
                  width: "25px",
                  left: "20px",
                  bottom: "10px",
                  zIndex: 999,
              }}
          >
              <img src={homeicon} />
          </Button>
      </div>
    );
  }
}

export default AntennaUtil;
