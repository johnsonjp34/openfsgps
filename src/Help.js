import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {Button} from "@material-ui/core";
import homeicon from "./homeicon.svg";



class Help extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: "MPEP 100",
            numPages: null,
            pageNumber: 1
        };

    }



    goHome = () => {
        this.props.history.push("/mainmenu");
    };




    render() {


        return (
            <div>
                <Card>
                    <CardContent>
                        <Typography color="textSecondary" gutterBottom>
                            How You Can Help.
                        </Typography>
                        <Typography variant="h5" component="h2"></Typography>

                        <Typography variant="body2" component="p">
                            This is an open source project. By purchasing a Dev Kit you help support the development efforts. Additionally,
                            please join the community at fsgps.plantermonitor.com. This project is maintained by Macon Apps / Investor Direction LLC
                        </Typography>

                        <Typography variant="body2" component="p">
                            This app is not designed to replace a GPS field navigation system. It is designed to help you remember where you did not spray. Please read through the terms of use of this app. If you do not agree with the terms you may not continue to use the app. Please contact us for a refund if this is the case. You agree to test this app for your specific purpose and to not continue to use it unless you find that it meets your criteria for accuracy and reliability for your specific application. You agree that GPS systems can have significant location error. You agree to use this application in a manner that considers safety concerns for yourself, others, and property. You acknowledge that this product contains no representations or warranties. You agree to not hold liable Investor Direction LLC or any other associated developer for any reliance on this product in any manner. Thank you for your understanding.
                        </Typography>

                        <Typography variant="body2" component="p">
                            v 1.12
                        </Typography>

                        <Typography variant="body2" component="p">
                            Your use is acceptance of the CC0 1.0 Universal license.
                        </Typography>

                    </CardContent>
                </Card>

                <Button
                    onClick={() => this.goHome()}
                    style={{
                        position: "absolute",
                        width: "25px",
                        left: "20px",
                        bottom: "10px",
                        zIndex: 999,
                    }}
                >
                    <img src={homeicon} />
                </Button>
            </div>
        );
    }
}

export default Help;
