import React, { Component } from "react";
import Keyboard from "react-simple-keyboard";
import "react-simple-keyboard/build/css/index.css";
import "leaflet/dist/leaflet.css";

import "./App.css";

import L from "leaflet";
import "leaflet-draw";
import "leaflet-draw/dist/leaflet.draw.css";

import "leaflet-geometryutil";
import { Button } from "@material-ui/core";

import homeicon from "./homeicon.svg";
import starticon from "./starticon.svg";
import settingsicon from "./settings.svg";
import pauseicon from "./pause.svg";
import InputAdornment from "@material-ui/core/InputAdornment";
import Input from "@material-ui/core/Input";

import { SphericalUtil, PolyUtil } from "node-geometry-library";
import { LatLng } from "leaflet/dist/leaflet-src.esm";

const fs = window.require("fs");
const homedir = window.require("os").homedir();

var attributionStr =
  "Esri, DigitalGlobe, GeoEye, i-cubed, USDA FSA, USGS, AEX, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community";

var map;

var polyLineList = [];

var currentpolyLineList = [];
var track = false;
var initialData = "empty";
var currentLat;
var currentLon;
var previousLat;
var previousLon;
var firstpolyline;
var trackingPolygonPoints;
var trackingPolygon;
var currentTrackingPolygon;
var stafieldName = "FieldName";
var gloWidth = 0;
var polygonCounter = 0;
var polyLineCounter = 0;
var FileNames = "";
var fileList = [];
var openFileData;

var gpsTemp = [];
var sessionTime;
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
  },
};

class MapTracking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lng: -82.6,
      lat: 39.7,
      zoom: 12,
      currentLocation: { lat: 39.723464, lng: -82.5993 },
      gpsData: "",
      showModal: "none",
      width: 0,
      fieldName: "FieldName",
      meters: 0,
      polygonNumber: 1,
      selectedFile: "",
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    console.log(this.state.polygonNumber)

  }

  componentWillUnmount() {
    track = false;
    fileList = [];
    FileNames = "";
  }
//only used for simulation
  timeDelay = (data, c) => {
    var self = this;
    setTimeout(function () {
      self.dataFeed(data[c]);
    }, 100 * c);
  };

  onChangeKeyBoard1 = (input) => {
    this.setState({ width: input });
  };

  onKeyPressKeyBoard1 = (button) => {
    console.log("Button pressed", button);
  };

  onChangeKeyBoard2 = (input) => {
    this.setState({ fieldName: input });
  };

  onKeyPressKeyBoard2 = (button) => {
    console.log("Button pressed", button);
  };

  closeModal = () => {
    this.setState({ showModal: "none" });
  };

  gpsTestDataInjection = () => {
    var self = this;

    fs.readFile("./gps-files/GPS.txt", "utf8", function (err, data) {
      // Display the file content
      console.log(data);
      data = JSON.parse(data);

      console.log(data.length);

      for (var c = 0; c < data.length; c++) {
        self.timeDelay(data, c);
      }
    });
  };

  storeGPSData = () => {
    //This is not getting updated.
    //Also need to create the directory

    setInterval(function () {
      if (track == true && gloWidth != 0 && stafieldName != "fieldName") {
        let gpsStr = JSON.stringify(gpsTemp);
        console.log("writing file");
        console.log(stafieldName);
        console.log(homedir);

        fs.mkdir(homedir + "/FSGPS/", { recursive: true }, (err) => {
          if (err) throw err;
        });

        fs.writeFile(
          homedir + "/FSGPS/" + stafieldName + sessionTime + ".txt",
          gpsStr,
          (err) => {
            if (err) console.log(err);
            console.log("Successfully Written to File.");
          }
        );
      }
    }, 1000 * 60 * 0.3); //5 min
  };

  saveGPSInfo = (gpsInfo) => {
    if (track === true) {
      gpsTemp.push(gpsInfo);
    }
  };

  componentDidMount() {
    //considering Mount a session for file naming scheme.
    let d = new Date();

    sessionTime = d.getMonth() + 1 + "-" + d.getDate() + "-" + d.getFullYear() + "-" + d.getHours() + "-" + d.getMinutes() + "-" + d.getSeconds()
    this.storeGPSData();

    var self = this;

    window.ipcRenderer.on("gpsinfo", (event, message) => {
      // console.log(message);
      self.dataFeed(message);

    });

    if (initialData === "empty") {
      self.activateMap(38.910559, -83.439735);
    } else if (initialData.lat == null) {
      self.activateMap(38.910559, -83.439735);
    } else {
      self.activateMap(initialData.lat, initialData.lon);
    }

    //input field takes the value of Feet
    let storedWidth = localStorage.getItem("width");
    if (storedWidth != null) {
      //calculations require meters.
      this.setState({ meters: storedWidth * 0.3048 });
      this.setState({ width: storedWidth });
    }

    try {
      fs.readdirSync(homedir + "/FSGPS/").forEach((file) => {
        fileList.push({ fileName: file });
        console.log(file);
      });
    } catch (e) {
      console.log("Nothing In Storage");
    }
    console.log(fileList);

    FileNames = () => <div> Nothing in Storage</div>;
  }

  dataFeed = (message) => {
    initialData = message;

    currentLat = message.lat;
    currentLon = message.lon;



    //add Field Name and polygon Number

    message.fieldsname = this.state.fieldName;
    message.polygonsNumber = this.state.polygonNumber;
    message.impWidth = this.state.meters;


    //don't store repeat locations.
    if (currentLat === previousLat && currentLon === previousLon) {
    } else {
      if (track === true) {

        this.setState({ gpsData: message });

        polyLineList.push(new L.LatLng(message.lat, message.lon));

        let position = new L.LatLng(message.lat, message.lon);

        currentpolyLineList.push({location : position, polygonSet: this.state.polygonNumber})

        console.log(currentpolyLineList)

        this.saveGPSInfo(message);


      }
    }

    previousLat = currentLat;
    previousLon = currentLon;
  };

  activateMap = (lat, long) => {
    var self = this;

    map = L.map("map").setView([lat, long], 13);

    L.tileLayer(
      "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
      {
        useCache: true,
        crossOrigin: true,
        attribution: attributionStr,
      }
    ).addTo(map);

    L.tileLayer(
      "https://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Transportation/MapServer/tile/{z}/{y}/{x}",
      {
        useCache: true,
        crossOrigin: true,
        attribution: attributionStr,
      }
    ).addTo(map);

    setTimeout(function () {
      if (initialData === "empty") {
      } else {
        map.setView([initialData.lat, initialData.lon], 17);
      }
    }, 3000);
  };

  onMode = (bool) => {

    if(this.state.fieldName == "FieldName") {
      alert("You need to name the field or event.")
    }
else {
      if (track == true) {
        track = false;
      } else if (track == false) {
        if (this.state.meters != 0) {
          track = true;
        } else {
          alert("Need to set width of path in settings.");
        }
      }
      console.log(track);
      this.setState({refresh: "abc"});

    }

  };

  onPause = (bool) => {
    if (track == true) {
      track = false;
    } else if (track == false) {
      if (this.state.meters != 0) {
        track = true;
      } else {
        alert("Need to set width of path in settings.");
      }
    }
    console.log(track);
    this.setState({ refresh: "abc" });

    this.setState({ polygonNumber: this.state.polygonNumber + 1 });
  };

  drawPolyline = () => {
    console.log(polyLineCounter);

    if (polyLineCounter > 2) {
      map.removeLayer(firstpolyline);
      console.log("removing polyline");
    }

    firstpolyline = new L.Polyline(polyLineList, {
      color: "red",
      weight: 1,
      opacity: 0.5,
      smoothFactor: 1,
    });

    firstpolyline.addTo(map);

    polyLineCounter++;
  };

  goHome = () => {
    this.props.history.push("/mainmenu");
  };

  polygonPathGenerator = () => {
    /*

{"speed":73.59848000000001,"lat":39.75868,"lon":-82.61755833333333,"satsActive":[18,20,23,24,15,10,32,13],"time":"2020-11-29T15:05:11.000Z","alt":312, polygonNumber:1, fieldName:"afield"},{"speed":73.59848000000001,"lat":39.75868,"lon":-82.61755833333333,"satsActive":[18,20,23,24,15,10,32,13],"time":"2020-11-29T15:05:11.000Z","alt":312}



       */
  };

  currentpolygonEngine = (path) => {


    polygonCounter++;


    //console.log("polygonEngine");
   // console.log(path)

     currentTrackingPolygon = L.polygon(path, {
      color: "yellow",
      opacity: "0.6",
      fillColor: "yellow",
      fillOpacity: "0.3",
    });
    currentTrackingPolygon.addTo(map);
  };

  polygonEngine = (path) => {



    console.log("polygonEngine");
    console.log(path)

    var resumeTrackingPolygon = L.polygon(path, {
      color: "yellow",
      opacity: "0.6",
      fillColor: "yellow",
      fillOpacity: "0.3",
    });
    resumeTrackingPolygon.addTo(map);
  };

  polygonCoordinateCalculator = (centerLinePoints) => {
    console.log("polygonCoordinateCalculator");

    let resumeTrackingPolygonPoints;
    let resumeTrackingPolygon;

    let leftArr = [];
    let rightArr = [];

    resumeTrackingPolygonPoints = [];

    //use polygonBreaks arg to know when to draw separate polygons. i.e pause / resume The breaks are the array index location where pause event occurred

    for (var p = 0; p < centerLinePoints.length - 1; p++) {
      let instantHeading = SphericalUtil.computeHeading(
        centerLinePoints[p],
        centerLinePoints[p + 1]
      );

      //compute point to the right and to the left at X meters

      let pointsToLeft = SphericalUtil.computeOffset(
        centerLinePoints[p],
        this.state.meters / 2,
        instantHeading + 90
      );

      leftArr.push(pointsToLeft);

      let pointsToRight = SphericalUtil.computeOffset(
        centerLinePoints[p],
        this.state.meters / 2,
        instantHeading + 270
      );

      rightArr.push(pointsToRight);
    }

    //combine the left and right array to make a whole polygon

    let reversedright = rightArr.reverse();

    resumeTrackingPolygonPoints = leftArr.concat(reversedright);

    this.polygonEngine(resumeTrackingPolygonPoints);
  };

  resumeDrawPolygon = (polygonBreaks) => {

    console.log(polyLineList)
    console.log(polygonBreaks)

    console.log("resumeDrawPolygon");

    console.log("polygon breaks " + polygonBreaks);
    if (polygonBreaks.length > 1) {
      for (var pbs = 0; pbs < polygonBreaks.length + 1; pbs++) {
        //loop through index markers. slice out polygon portion and send out for drawing

        if(pbs == 0) {
          console.log(polygonBreaks[0] - 1)
          this.polygonCoordinateCalculator(
              polyLineList.slice(0, polygonBreaks[0] - 1)
          );
        }
        else if(pbs > 0 && pbs < polygonBreaks.length) {
console.log(polygonBreaks[pbs - 1])
console.log(polygonBreaks[pbs] - 1)
          this.polygonCoordinateCalculator(
              polyLineList.slice(polygonBreaks[pbs - 1], polygonBreaks[pbs] - 1)
          );
        }

        else if(pbs = polygonBreaks.length){
          console.log(polygonBreaks[pbs - 1])
          console.log(polyLineList.length)
          this.polygonCoordinateCalculator(polyLineList.slice(polygonBreaks[pbs - 1],polyLineList.length));
        }

      }
    } else {
      this.polygonCoordinateCalculator(polyLineList);
    }
  };

  drawPolygon = () => {

    if (polygonCounter >0 ) {
      map.removeLayer(currentTrackingPolygon);
      console.log("Should purge layers")
      let i = 0;
      for(i in map._layers) {
        if(map._layers[i]._path != undefined) {
          try {
            map.removeLayer(map._layers[i]);
          }
          catch(e) {
            console.log("problem with " + e + map._layers[i]);
          }
        }
      }

    }

    let currentpolygonBreaks = [];

    console.log(map._layers);
    //keep an eye on number of map layers

    //trackingPolygon = null;


    let leftArr = [];
    let rightArr = [];

    trackingPolygonPoints = [];

   // if (currentpolyLineList.length > 2) {



      for (var df = 0; df < currentpolyLineList.length; df++) {
        if (df > 1) {
          if (
              currentpolyLineList[df].polygonSet >
              currentpolyLineList[df - 1].polygonSet
          ) {
            currentpolygonBreaks.push(df);
          }
        }

      }

      console.log(currentpolygonBreaks.length)

      if (currentpolygonBreaks.length > 0) {
        for (var pbs = 0; pbs < currentpolygonBreaks.length + 1; pbs++) {
          //loop through index markers. slice out polygon portion and send out for drawing

          if (pbs == 0) {
            console.log(currentpolygonBreaks[0] - 1)
            let coordinates = [];
            for(var q = 0; q<currentpolygonBreaks[0] - 1; q++){
              coordinates.push(currentpolyLineList[q].location)
            }
          console.log(coordinates)
            this.currentPolygonCoordinateCalculator(
               coordinates
            );
          } else if (pbs > 0 && pbs < currentpolygonBreaks.length) {
            console.log(currentpolygonBreaks[pbs - 1]) //21
            console.log(currentpolygonBreaks[pbs] - 1) //20
            let coordinates1 = [];
            for(var q1 = 0;q1<currentpolyLineList.length ; q1++){
              if(q1<currentpolygonBreaks[pbs] - 1 && q1 > currentpolygonBreaks[pbs - 1]) {
               // console.log("loop executing " + q1 + " " + currentpolyLineList[q1].location)
                coordinates1.push(currentpolyLineList[q1].location)
              }
            }
            console.log(coordinates1) //empty?
            if(coordinates1.length == 0){
              console.log("no coordinates being pushed")
            }
            else{
            this.currentPolygonCoordinateCalculator(

                coordinates1

            );}
          } else if (pbs = currentpolygonBreaks.length) {
            console.log(currentpolygonBreaks[pbs - 1])
            console.log(currentpolyLineList.length)

            let coordinates2 = [];
            for(var q2 = 0; q2 < currentpolyLineList.length ; q2++){
              if(q2<currentpolyLineList.length && q2 > currentpolygonBreaks[pbs - 1]) {
                coordinates2.push(currentpolyLineList[q2].location)
              }

            }

            //console.log(coordinates2); //empty??

            this.currentPolygonCoordinateCalculator(coordinates2);
          }

        }
      }

      else {

        let coordinates3 = [];
        for(var q3 = 0; q3<currentpolyLineList.length ; q3++){
          coordinates3.push(currentpolyLineList[q3].location)
        }
        console.log(coordinates3);
        this.currentPolygonCoordinateCalculator(coordinates3);

      }


    //}
  };

  currentPolygonCoordinateCalculator = (centerLinePoints) => {

    console.log("polygonCoordinateCalculator");

    trackingPolygonPoints = [];
    let resumeTrackingPolygon;

    let leftArr = [];
    let rightArr = [];

//console.log(centerLinePoints)

    //use polygonBreaks arg to know when to draw separate polygons. i.e pause / resume The breaks are the array index location where pause event occurred

    for (var p = 0; p < centerLinePoints.length - 1; p++) {
      let instantHeading = SphericalUtil.computeHeading(
          centerLinePoints[p],
          centerLinePoints[p + 1]
      );

      //compute point to the right and to the left at X meters

      let pointsToLeft = SphericalUtil.computeOffset(
          centerLinePoints[p],
          this.state.meters / 2,
          instantHeading + 90
      );

      leftArr.push(pointsToLeft);

      let pointsToRight = SphericalUtil.computeOffset(
          centerLinePoints[p],
          this.state.meters / 2,
          instantHeading + 270
      );

      rightArr.push(pointsToRight);
    }

    //combine the left and right array to make a whole polygon

    let reversedright = rightArr.reverse();

    trackingPolygonPoints = leftArr.concat(reversedright);

    this.currentpolygonEngine(trackingPolygonPoints);

  }

  settingsAction = () => {
    if (fileList.length != 0) {
      FileNames = fileList.map((filenames, index) => (
        <Button
          onClick={() => this.setState({ selectedFile: filenames.fileName })}
          style={{
            backgroundColor: "grey",
            marginBottom: "5px",
            marginLeft: "5px",
            marginRight: "5px",
            display: "block",
          }}
          key={index}
        >
          File {filenames.fileName}
        </Button>
      ));
    }
    console.log("Settings fired.");

    this.setState({ showModal: "block" });
  };

  setWidthandFieldName = () => {
    localStorage.setItem("width", this.state.width);
    localStorage.setItem("fieldName", this.state.fieldName);

    this.setState({ meters: this.state.width * 0.3048 });

    this.setState({ showModal: "none" });

    console.log(this.state.width);
  };

  openPrevious = () => {
    //[{"speed":0,"lat":39.761255,"lon":-82.61544666666667,"satsActive":[26,3,31,22,29,32,27],"time":"2021-01-02T19:22:11.000Z","alt":324.1,"fieldsname":"taco","polygonsNumber":1,"impWidth":30.48}, ... ]
    console.log("Open Previous");
    try {
      const data = fs.readFileSync(
        homedir + "/FSGPS/" + this.state.selectedFile,
        "utf8"
      );
      //console.log(data);
      console.log(typeof data);
      openFileData = JSON.parse(data);
      console.log(typeof openFileData);
      console.log(openFileData)
      this.populateGPSDataFromStorage();
    } catch (err) {
      console.error(err);
    }

    this.setState({ showModal: "none" });
  };

  populateGPSDataFromStorage = () => {
    console.log("populateGPSDataFromStorage");
    let polygonBreaks = [];

    try {
      polyLineList = [];

      console.log("Restoring Field Info");

      console.log(openFileData.length);
      console.log(openFileData[2].lat);

      this.setState({fieldName: openFileData[2].fieldsname})

      this.setState({
        polygonNumber: openFileData[openFileData.length - 1].polygonsNumber + 1,
      });

      for (var df = 0; df < openFileData.length; df++) {
        if (df > 1) {
          if (
            openFileData[df].polygonsNumber >
            openFileData[df - 1].polygonsNumber
          ) {
            polygonBreaks.push(df);
          }
        }

        polyLineList.push(
          new L.LatLng(openFileData[df].lat, openFileData[df].lon)
        );

        currentpolyLineList.push(
            {location: new L.LatLng(openFileData[df].lat, openFileData[df].lon), polygonSet: openFileData[df].polygonsNumber}
        );

        let objToCopyIntoStorage = {

          lat: openFileData[df].lat,
          lon: openFileData[df].lon,
          time: openFileData[df].time,
          alt: openFileData[df].alt,
          fieldsname: openFileData[df].fieldsname,
          polygonsNumber: openFileData[df].polygonsNumber,
          impWidth: openFileData[df].impWidth


        }

        gpsTemp.push(objToCopyIntoStorage);

        if (df == 1) {
          this.setState({ meters: openFileData[df].impWidth });
        }
      }
    } catch (e) {
      console.log(e);
    }

    this.resumeDrawPolygon(polygonBreaks);
  };

  render() {
    //console.log(polyLineList);
    /*

*/
    stafieldName = this.state.fieldName;
    gloWidth = this.state.meters;

    //constantly update map with polygon and present location
    if (track === true) {
      //  map.setView([this.state.gpsData.lat, this.state.gpsData.lon]);

      //uncomment when not using test file.
      this.drawPolygon();
     // this.drawPolyline();
    }
    let StartButton;

    if (track == false) {
      StartButton = () => (
        <Button
          onClick={() => this.onMode(true)}
          style={{
            position: "absolute",
            width: "25px",
            left: "20px",
            bottom: "150px",
            zIndex: 997,
          }}
        >
          <img src={starticon} />
        </Button>
      );
    } else {
      StartButton = () => (
        <Button
          onClick={() => this.onPause(false)}
          style={{
            position: "absolute",
            width: "25px",
            left: "20px",
            bottom: "150px",
            zIndex: 997,
          }}
        >
          <img src={pauseicon} />
        </Button>
      );
    }

    return (
      <div>
        <div id="map" style={{ zIndex: 1 }} />

        <StartButton />

        <Button
          onClick={() => this.goHome()}
          style={{
            position: "absolute",
            width: "25px",
            left: "20px",
            bottom: "10px",
            zIndex: 997,
          }}
        >
          <img src={homeicon} />
        </Button>

        <div
          style={{
            display: this.state.showModal,
            position: "absolute",
            top: "5px",
            left: "50%",
            zIndex: 999,
            borderStyle: "solid",
            backgroundColor: "white",
          }}
        >
          <center>
            <div>
              <Input
                placeholder={this.state.width}
                onChange={(e) => this.setState({ width: e.target.value })}
                label="Width in Feet"
                style={{
                  borderStyle: "solid",
                  display: "block",
                  margin: "5px 5px 5px 5px",
                  padding: "5px 5px 5px 5px",
                }}
                endAdornment={
                  <InputAdornment position="end">Feet</InputAdornment>
                }
                inputProps={{
                  "aria-label": "Implement Width",
                }}
              ></Input>
              <Keyboard
                  display={{
                    '{bksp}': '<-'}}
                layout={{ default: ["1 2 3 4 5", "6 7 8 9 0 . {bksp}"] }}
                onChange={this.onChangeKeyBoard1}
                onKeyPress={this.onKeyPressKeyBoard1}
              />
              <Input
                placeholder={this.state.fieldName}
                onChange={(e) => this.setState({ fieldName: e.target.value })}
                label="Field Name"
                style={{
                  borderStyle: "solid",
                  display: "block",
                  margin: "5px 5px 5px 5px",
                  padding: "5px 5px 5px 5px",
                }}
                endAdornment={
                  <InputAdornment position="end">Field Name</InputAdornment>
                }
              ></Input>
              <Keyboard
                  display={{
                    '{bksp}': '<-'}}
                layout={{
                  default: [
                    "a b c d e f g h i j",
                    "k l m n o p q r s t",
                    "u v w x y z {bksp}",
                  ],
                }}
                onChange={this.onChangeKeyBoard2}
                onKeyPress={this.onKeyPressKeyBoard2}
                baseClass={"keyboard2"}
              />
              <Button
                style={{
                  border: "2px solid black",
                  marginRight: "15px",
                  marginBottom: "5px",
                }}
                onClick={() => this.closeModal()}
              >
                Cancel
              </Button>
              <Button
                style={{ border: "2px solid black", marginBottom: "5px" }}
                onClick={() => this.setWidthandFieldName()}
              >
                OK
              </Button>
            </div>
          </center>
        </div>

        <div
          style={{
            display: this.state.showModal,
            position: "absolute",
            top: "10px",
            left: "10%",
            zIndex: 999,
            borderStyle: "solid",
            backgroundColor: "white",
          }}
        >
          <center>
            <div >
              <h3>Open Previous</h3>
              <h2>{this.state.selectedFile}</h2>

              <div style={{overflow: "scroll", height: "350px"}}>{FileNames}</div>

              <Button
                style={{
                  border: "2px solid black",
                  marginRight: "15px",
                  marginBottom: "10px",
                }}
                onClick={() => this.closeModal()}
              >
                Cancel
              </Button>
              <Button
                style={{ border: "2px solid black", marginBottom: "10px" }}
                onClick={() => this.openPrevious()}
              >
                OK
              </Button>
            </div>
          </center>
        </div>

        <Button
          style={{
            width: "50px",
            position: "absolute",
            top: "10px",
            right: "25px",
            zIndex: 999,
            borderColor: "white",
          }}
          onClick={() => this.settingsAction()}
        >
          <img src={settingsicon} />
        </Button>
      </div>
    );
  }
}

export default MapTracking;
