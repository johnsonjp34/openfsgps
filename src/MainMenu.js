import React, { Component } from "react";
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

class MainMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNumber: 1,
    };
  }

  componentDidMount() {
    try {
      var portPath = localStorage.getItem("device");
      window.ipcRenderer.send("gpsselected", portPath, 10);
    } catch (e) {
      console.log("Unable to connect or find device");
    }
  }

  acreToolNav = () => {
    this.props.history.push("/acretool");
    console.log("acre tool");
  };

  mapTrackingNav = () => {
    this.props.history.push("/maptracking");
    console.log("map tracking mode");
  };

  gpsDeviceEnable = () => {
    this.props.history.push("/antennautil");
  };

  navHelp = () => {
    this.props.history.push("/help");
  };

  render() {
    return (
      <div style={{ position: "absolute", top: "30%", paddingLeft: "10%" }}>
        <Button onClick={() => this.acreToolNav()}>
          <Card>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                Measurement Tools
              </Typography>
              <Typography variant="h5" component="h2"></Typography>

              <Typography variant="body2" component="p">
                Distance & Field Area
              </Typography>
            </CardContent>
          </Card>
        </Button>

        <Button onClick={() => this.mapTrackingNav()}>
          <Card>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                Map Tracking
              </Typography>
              <Typography variant="h5" component="h2"></Typography>

              <Typography variant="body2" component="p">
                Live map highlighting with GPS.
              </Typography>
            </CardContent>
          </Card>
        </Button>

        <Button onClick={() => this.gpsDeviceEnable()}>
          <Card>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                GPS Settings
              </Typography>
              <Typography variant="h5" component="h2"></Typography>

              <Typography variant="body2" component="p">
                Enable your GPS device.
              </Typography>
            </CardContent>
          </Card>
        </Button>

        <Button onClick={() => this.navHelp()}>
          <Card>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                About & Support
              </Typography>
              <Typography variant="h5" component="h2"></Typography>

              <Typography variant="body2" component="p">
                How you can help.
              </Typography>
            </CardContent>
          </Card>
        </Button>
      </div>
    );
  }
}

export default MainMenu;
